import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Apollo, ApolloModule } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { RestLink } from 'apollo-link-rest';
import { ApolloLink } from 'apollo-link';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ApolloModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(apollo: Apollo) {
    const cache = new InMemoryCache();

    const restLink = new RestLink({
      uri: 'api/',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    apollo.create({
      link: ApolloLink.from([restLink]),
      cache
    });
  }
}
