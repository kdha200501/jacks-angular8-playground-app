import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { QueryRef } from 'apollo-angular/QueryRef';
import { FetchResult } from 'apollo-link';

import { TodoServiceModule } from './todo-service.module';
import { ListTodos, UpdateTodo } from '../typings/types.local';
import { listTodosQuery, updateTodoMutation } from './todo.graphql';

@Injectable({
  providedIn: TodoServiceModule
})
export class TodoService {
  constructor(private apollo: Apollo) {}

  list(): QueryRef<ListTodos.Query, ListTodos.Variables> {
    return this.apollo.watchQuery<ListTodos.Query, ListTodos.Variables>({
      query: listTodosQuery,
      fetchPolicy: 'network-only'
    });
  }

  update(
    id: string,
    title: string,
    completed: boolean
  ): Observable<FetchResult<UpdateTodo.Mutation>> {
    return this.apollo.mutate<UpdateTodo.Mutation, UpdateTodo.Variables>({
      mutation: updateTodoMutation,
      variables: {
        id,
        input: {
          todo: {
            title,
            completed
          }
        }
      }
    });
  }
}
