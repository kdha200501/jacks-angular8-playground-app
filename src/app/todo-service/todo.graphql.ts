import gql from 'graphql-tag';

export const listTodosQuery = gql`
  query listTodos {
    listTodos @rest(type: "TodosResponseType", path: "todos") {
      todos @type(name: "TodoType") {
        id
        __typename
        title
        completed
      }
    }
  }
`;

export const updateTodoMutation = gql`
  mutation updateTodo($id: String!, $input: TodoPayloadInputType!) {
    updateTodo(id: $id, input: $input)
      @rest(type: "TodoResponseType", path: "todo/:id", method: "PUT") {
      todo @type(name: "TodoType") {
        id
        __typename
        title
        completed
      }
    }
  }
`;
