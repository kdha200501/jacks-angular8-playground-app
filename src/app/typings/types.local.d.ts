export type Maybe<T> = T | null;

export interface TodoPayloadInputType {
  todo: TodoInputType;
}

export interface TodoInputType {
  title: string;

  completed: boolean;
}

// ====================================================
// Documents
// ====================================================

export namespace ListTodos {
  export type Variables = {};

  export type Query = {
    __typename?: 'Query';

    listTodos: ListTodos;
  };

  export type ListTodos = {
    __typename?: 'TodosResponseType';

    todos: Maybe<Todos[]>;
  };

  export type Todos = {
    __typename: 'TodoType';

    id: string;

    title: string;

    completed: boolean;
  };
}

export namespace UpdateTodo {
  export type Variables = {
    id: string;
    input: TodoPayloadInputType;
  };

  export type Mutation = {
    __typename?: 'Mutation';

    updateTodo: UpdateTodo;
  };

  export type UpdateTodo = {
    __typename?: 'TodoResponseType';

    todo: Maybe<Todo>;
  };

  export type Todo = {
    __typename: 'TodoType';

    id: string;

    title: string;

    completed: boolean;
  };
}
