import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { TodoService } from '../todo-service/todo.service';
import { ListTodos } from '../typings/types.local';
import ITodo = ListTodos.Todos;

@Component({
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent implements OnInit {
  todos$: Observable<ITodo[]>;

  constructor(private _todoService: TodoService) {}

  ngOnInit(): void {
    this.todos$ = this._todoService.list().valueChanges.pipe(
      map(
        ({
          data: {
            listTodos: { todos }
          }
        }) => todos
      )
    );
  }

  identifyTodo(index: number, todo: ITodo): string {
    return todo.id;
  }
}
