import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoServiceModule } from '../todo-service/todo-service.module';
import { TodoComponent } from './todo.component';
import { TodoRoutingModule } from './todo-routing.module';
import { FormsModule } from '@angular/forms';
import { TodoItemComponent } from './components/todo-item/todo-item.component';

@NgModule({
  declarations: [TodoComponent, TodoItemComponent],
  entryComponents: [TodoComponent],
  imports: [CommonModule, TodoServiceModule, TodoRoutingModule, FormsModule]
})
export class TodoModule {}
