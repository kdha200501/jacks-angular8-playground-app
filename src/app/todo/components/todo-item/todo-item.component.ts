import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input
} from '@angular/core';
import { take } from 'rxjs/operators';

import { TodoService } from '../../../todo-service/todo.service';
import { ListTodos } from '../../../typings/types.local';
import ITodo = ListTodos.Todos;

@Component({
  selector: 'jd-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoItemComponent {
  @Input() todo: ITodo;

  constructor(
    private _todoService: TodoService,
    private _cdr: ChangeDetectorRef
  ) {}

  toggleCompleted(): void {
    this._submit(!this.todo.completed, this.todo.title);
  }

  updateTitle(event: Event): void {
    this._submit(this.todo.completed, (event.target as HTMLInputElement).value);
  }

  private _submit(completed: boolean, title: string): void {
    this._todoService
      .update(this.todo.id, title, completed)
      .pipe(take(1))
      .subscribe();
  }
}
